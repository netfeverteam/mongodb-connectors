# mongodb-connectors

This project is given to provide a set of connectors for authenticate and authorize application in a Java/J2EE world.

## Environment
* JDK >= 1.7
* MongoDB >= 3.4.0

## Binaries

MongoDB Connectors for J2SE & J2EE signin. You can take the latest tree or use the libraries for 
[JAAS](../../downloads/jaas-loginmodule-latest.zip) or [Tomcat 8.X/9.X](../../downloads/tomcat-realm-latest.zip)  

## Drivers


## Setting Test Mongo Database
Using mongo shell, or mongodb in general is outside the scope of this document. We assume that you know it... even if
stackoverflow is your best friend!
 
To work with the examples we are going to set user tables: one with plain text password and one with md5 hashes instead 
of password 

```bash
use usertest

db.users.insert( { username: "user1", password: "123456", roles: ["user" ] } );
db.users.insert( { username: "admin", password: "123456", roles: ["user", "admin"] } );
db.users.insert( { username: "user2", password: "123456", roles: ["user"], password: "654321" } );

db.userswithmd5.insert( { username: "user1", password: "e10adc3949ba59abbe56e057f20f883e", roles: ["user" ] } );
db.userswithmd5.insert( { username: "admin", password: "e10adc3949ba59abbe56e057f20f883e", roles: ["user", "admin"] } );
db.userswithmd5.insert( { username: "user2", password: "e10adc3949ba59abbe56e057f20f883e", roles: ["user" ], password2: "c33367701511b4f6020ec61ded352059" } );
```

## Configuration
The configuration can be define at the following 3 levels in order of override (the latter being the actual value)

1. In the home folder at __System.getProperty("user.home") + "/.mongodb-connectors/config.properties"__
2. As actual environment variables
3. In the J2EE application server

The variable names are as follows

| Variable Name     | Description                                                                                           | 
| ------------------|-------------------------------------------------------------------------------------------------------|
| MONGO_DB_HOST     | The mongodb server hostname. This can be an IP address or a domain name. By default it is *127.0.0.1* |
| MONGO_DB_PORT     | The mongodb server port. By default it is *27017*                                                     |
| MONGO_DB_USERNAME | The mongodb server user name. By default it is *null* (ie: no user specify on connection)             |
| MONGO_DB_PASSWORD | The mongodb server password. By default it is *null* (ie: no user specify on connection)              |

### Configuration Example
As mentioned earlier, the configuration can be define at the following 3 levels which is explained here from the lowest 
priority to the highest.

#### Properties in file 
First you can add a file in your home folder inside a <b>.mongodb-connectors</b> folder and a file named 
<b>config.properties</b>? This is a secure option as only the mongodb system user (and maybe group) and root can see 
this file

```text
MONGO_DB_HOST=localhost
MONGO_DB_PORT=27017
```

### Properties in environment variables
Secondly you can set the properties as environment variables.
 
```bash
export MONGO_DB_HOST=localhost
export MONGO_DB_PORT=27017
```

#### Properties in J2EE server
At this point it becomes difficult to provide a single set of examples but there are wiki pages for different 
application servers on this git repository.

## Examples
* [Tomcat with JAAS LoginModule](examples/tomcat/login-module-jaas) 
* [Tomcat with Realm](examples/tomcat/tomcat) 

## Licence

```text
Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
