/* Copyright (C) 2017 Yoram Halberstam.- All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Yoram Halberstam <yoram.halberstam@sgmail.com>
 */
package me.yoram.commons.tomcat.realm;

import me.yoram.commons.mongodb.MongoDbWrapper;
import me.yoram.commons.mongodb.Utils;
import org.apache.catalina.LifecycleException;
import org.apache.catalina.realm.GenericPrincipal;
import org.apache.catalina.realm.RealmBase;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Yoram Halberstam <yoram.halberstam@gmail.com>
 * @since 13/11/17
 */
public class MongoDbRealm extends RealmBase {
    private static final Logger LOG = Utils.getLogger(MongoDbRealm.class);

    // db credentials
    private String mongoHost;
    private int mongoPort = -1;
    private String mongoUser;

    private String mongoPass;

    // set from outside - connect to this db/connection/fields
    private String database;
    private String collection;
    private String userField;
    private String passwordField;
    private String roleField;

    private MongoDbWrapper wrapper;

    public String getMongoHost() {
        return mongoHost;
    }

    public void setMongoHost(String mongoHost) {
        this.mongoHost = mongoHost;
    }

    public int getMongoPort() {
        return mongoPort;
    }

    public void setMongoPort(int mongoPort) {
        this.mongoPort = mongoPort;
    }

    public String getMongoUser() {
        return mongoUser;
    }

    public void setMongoUser(String mongoUser) {
        this.mongoUser = mongoUser;
    }

    public String getMongoPass() {
        return mongoPass;
    }

    public void setMongoPass(String mongoPass) {
        this.mongoPass = mongoPass;
    }

    public String getDatabase() {
        return database;
    }

    public void setDatabase(String database) {
        this.database = database;
    }

    public String getCollection() {
        return collection;
    }

    public void setCollection(String collection) {
        this.collection = collection;
    }

    public String getUserField() {
        return userField;
    }

    public void setUserField(String userField) {
        this.userField = userField;
    }

    public String getPasswordField() {
        return passwordField;
    }

    public void setPasswordField(String passwordField) {
        this.passwordField = passwordField;
    }

    public String getRoleField() {
        return roleField;
    }

    public void setRoleField(String roleField) {
        this.roleField = roleField;
    }

    @Override
    protected void startInternal() throws LifecycleException {
        if (wrapper != null) {
            wrapper.stop();
            wrapper = null;
        }

        wrapper = new MongoDbWrapper(
                mongoHost,
                mongoPort,
                mongoUser,
                mongoPass,
                database,
                collection,
                userField,
                passwordField,
                roleField,
                null,
                false);

        wrapper.start();

        super.startInternal();
    }

    @Override
    protected void stopInternal() throws LifecycleException {
        LOG.info("stopping MongoRealm");

        if (wrapper != null) {
            wrapper.stop();
            wrapper = null;
        }

        super.stopInternal();
    }

    @Override
    protected String getName() {
        return this.getClass().getSimpleName();
    }

    @Override
    protected String getPassword(final String username) {
        final String[] passwords = wrapper.getPasswords(username);

        if (passwords != null && passwords.length > 1) {
            try {
                throw new Exception(
                        String.format(
                                "More than one password field found for username [%s] but the method only expect 1. It looks like we forgot to code it.",
                                username));
            } catch (Exception e) {
                LOG.log(Level.WARNING, e.getMessage(), e);
            }
        }

        return passwords == null || passwords.length == 0 ? null : passwords[0];
    }

    @Override
    protected Principal getPrincipal(final String username) {
        return (new GenericPrincipal(username,
                getPassword(username),
                getRole(username)));
    }

    private List<String> getRole(String username) {
        LOG.log(Level.INFO, "getting role for {0}", username);
        return new ArrayList<>(wrapper.getRoles(username));
    }

    @Override
    public Principal authenticate(String username, String credentials) {
        final String[] passwords = wrapper.getPasswords(username);

        if (passwords != null) {
            for (final String serverCredentials: passwords) {
                boolean validated = getCredentialHandler().matches(credentials, serverCredentials);

                if (validated) {
                    if (containerLog.isTraceEnabled()) {
                        containerLog.trace(sm.getString("realmBase.authenticateSuccess", username));
                    }

                    return new GenericPrincipal(username, serverCredentials, getRole(username));
                }
            }
        }

        if (containerLog.isTraceEnabled()) {
            containerLog.trace(sm.getString("realmBase.authenticateFailure", username));
        }

        return null;
    }
}
