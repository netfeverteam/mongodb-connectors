## Configuration

Install the latest [Tomcat 8.X/9.X](../../downloads/tomcat-realm-latest.zip) libraries to ``<CATALINA_HOME>/lib``

Add the MongoDB Realm in <CATALINA_HOME>/conf/server.xml. You may need to disable/comment out other <Realm>

```xml
<Server port="8005" shutdown="SHUTDOWN">
   ...
  <Service name="Catalina">
    ...
    <Engine name="Catalina" defaultHost="localhost">
        ...
	    <Realm className="org.apache.catalina.realm.LockOutRealm">
		    <Realm
			    className="me.yoram.commons.tomcat.realm.MongoDbRealm"
			    database="usertest"
			    collection="userswithmd5"
			    userField="username"
			    passwordField="password"
			    roleField="roles">
			    
			    <!-- This can be added if the password is hashed in the database. The example below assumes it is md5 -->
			    <CredentialHandler className="org.apache.catalina.realm.MessageDigestCredentialHandler" algorithm="md5" />
		    </Realm>

            <!-- 
                Alternative with 2 passwords column. 2 Passwords columns is useful when the admin wants to impersonate 
                the account of a customer without creating an impersonation framework. Essentially his account/password 
                and his account/your password will work.
            -->
            <!--
		    <Realm
			    className="me.yoram.commons.tomcat.realm.MongoDbRealm"
			    database="usertest"
			    collection="userswithmd5"
			    userField="username"
			    passwordField="password,password2"
			    roleField="roles">
			    
			    <CredentialHandler className="org.apache.catalina.realm.MessageDigestCredentialHandler" algorithm="md5" />
		    </Realm>
            -->
        </Realm>
        ...
    </Engine>
  </Service>
</Server>
```

