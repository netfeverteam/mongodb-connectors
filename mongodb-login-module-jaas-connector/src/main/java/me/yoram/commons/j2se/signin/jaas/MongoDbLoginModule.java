/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.commons.j2se.signin.jaas;

import me.yoram.commons.mongodb.Config;
import me.yoram.commons.mongodb.MongoDbWrapper;
import me.yoram.commons.mongodb.Utils;

import javax.security.auth.Subject;
import javax.security.auth.callback.*;
import javax.security.auth.login.LoginException;
import javax.security.auth.spi.LoginModule;
import java.io.IOException;
import java.util.Collection;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 12/09/18
 */
public class MongoDbLoginModule implements LoginModule {
    private static final Logger LOGGER = Utils.getLogger(MongoDbLoginModule.class);

    private MongoDbWrapper wrapper;
    private Subject subject;
    private CallbackHandler callbackHandler;
    private Map<String, ?> sharedState;
    private Map<String, ?> options;
    private boolean logInitialize = true;

    private String getOption(final String key) {
        return this.options == null ? null : (String)this.options.get(key);
    }

    String getHost() {
        return getOption(Config.KEY_HOST);
    }

    int getPort() {
        final String s = getOption(Config.KEY_PORT);
        final int res = s == null ? -1 : Integer.parseInt(s);

        return res < 1 ? -1 : res;
    }

    String getUsername() {
        return getOption(Config.KEY_USER);
    }

    String getPassword() {
        return getOption(Config.KEY_PASS);
    }

    String getAuthDB() {
        return getOption("database");
    }

    String getAuthCollection() {
        return getOption("collection");
    }

    String getDigest() {
        return getOption("digest");
    }

    String getAuthUserField() {
        return getOption("userField");
    }

    String getAuthPasswordField() {
        return getOption("passwordField");
    }

    String getAuthRoleField() {
        return getOption("roleField");
    }

    boolean isDebug() {
        return "true".equalsIgnoreCase(getOption("debug"));
    }

    @Override
    public void initialize(
            final Subject subject,
            final CallbackHandler callbackHandler,
            final Map<String, ?> sharedState,
            final Map<String, ?> options) {
        this.subject = subject;
        this.callbackHandler = callbackHandler;
        this.sharedState = sharedState;
        this.options = options;

        if (logInitialize || isDebug()) {
            final StringBuilder sb = new StringBuilder("Initializing LoginModule ")
                    .append(this.getClass().getName())
                    .append(" with config host=")
                    .append(getHost())
                    .append(" with config username=")
                    .append(getUsername() == null ? "" : getUsername())
                    .append(" with config password=")
                    .append(getPassword() == null ? "" : "****")
                    .append("; authDB=")
                    .append(getAuthDB())
                    .append("; authCollection=")
                    .append(getAuthCollection())
                    .append("; digest=")
                    .append(getDigest())
                    .append("; authUserField")
                    .append(getAuthUserField())
                    .append("; authPasswordField")
                    .append(getAuthPasswordField())
                    .append("; authRoleField")
                    .append(getAuthRoleField())
                    .append("; isDebug=")
                    .append(isDebug());

            LOGGER.info(sb.toString());

            logInitialize = false;
        }

        this.wrapper = new MongoDbWrapper(
                getHost(),
                getPort(),
                getUsername(),
                getPassword(),
                getAuthDB(),
                getAuthCollection(),
                getAuthUserField(),
                getAuthPasswordField(),
                getAuthRoleField(),
                getDigest(),
                true);
        wrapper.start();
    }

    @Override
    public boolean login() throws LoginException {
        System.out.println("login()");
        NameCallback nameCb = new NameCallback("_name_");
        PasswordCallback passwordCb = new PasswordCallback("_password_", false);

        try {
            this.callbackHandler.handle(new Callback[] {nameCb, passwordCb});
            System.out.println(nameCb.getName());

            Collection<String> roles = wrapper.getRoles(nameCb.getName(), new String(passwordCb.getPassword()));

            if (roles == null) {
                return false;
            }

            subject.getPrincipals().add(new UserPrincipal(nameCb.getName()));

            for (final String role: roles) {
                subject.getPrincipals().add(new RolePrincipal(role));
            }

            return true;
        } catch (UnsupportedCallbackException | IOException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            throw new LoginException(e.getMessage());
        }
    }

    @Override
    public boolean commit() {
        return true;
    }

    @Override
    public boolean abort() {
        return true;
    }

    @Override
    public boolean logout() {
        subject.getPrincipals().clear();
        return true;
    }
}
