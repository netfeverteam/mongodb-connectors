/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.commons.mongodb;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Objects;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 13/09/18
 */
public class TestUtils {
    @DataProvider
    public Object[][] dpGenerateId() {
        return new Object[][] {
                {null, ""},
                {new String[] {"x"}, "/x"},
                {new String[] {"x", null}, "/x/"},
                {new String[] {null, "x", null}, "//x/"},
                {new String[] {null, "x", null, "y"}, "//x//y"}
        };
    }

    @Test(dataProvider = "dpGenerateId")
    public void testGenerateId(String[] data, String expectedResult) {
        String res = Utils.generateId(data);

        assert Objects.equals(res, expectedResult) : String.format("Expected %s but %s returned.", expectedResult, res);
    }
}
