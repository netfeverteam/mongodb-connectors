/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.commons.mongodb;

import com.mongodb.ServerAddress;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The configuration can be define at the following 3 levels in order of override (the latter being the actual value)
 * <ol>
 *     <li>
 *         In the home folder at <code>System.getProperty("user.home") + "/.mongodb-connectors/config.properties"</code>
 *     </li>
 *     <li>As actual environment variables</li>
 *     <li>In the J2EE application server</li>
 * </ol>
 *
 * The variable names are as follows
 * <ul>
 *     <li>
 *         <b>MONGO_DB_HOST</b> - The mongodb hostname. This can be an IP address or a domain name. By default it is
 *         localhost.
 *     </li>
 *     <li>
 *         <b>MONGO_DB_PORT</b> - The mongodb hostname. This can be an IP address or a domain name. By default it is
 *         localhost.
 *     </li>
 *     MONGO_DB_USERNAME";
 *     private static final String KEY_PASS = "MONGO_DB_PASSWORD";
 *
 *
 *
 * first get it from property file
 * then get it from env variable
 * then from default
 *
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 13/09/18
 */
public class Config {
    private static final Logger LOG = Utils.getLogger(Config.class);

    public static final String KEY_HOST = "MONGO_DB_HOST";
    public static final String KEY_PORT = "MONGO_DB_PORT";
    public static final String KEY_USER = "MONGO_DB_USERNAME";
    public static final String KEY_PASS = "MONGO_DB_PASSWORD";

    private static Properties props;

    /**
     * @return property file are at System.getProperty("user.home") + "/.mongodb-connectors/config.properties'
     */
    static Properties getProperties() {
        if (props == null) {
            final File f = new File(
                    String.format("%s" + "/.mongodb-connectors/config.properties", System.getProperty("user.home")));

            LOG.log(
                    Level.CONFIG,
                    String.format(
                            "Looking for mongodb-connectos properties at %s (%s)",
                            f.getAbsolutePath(),
                            f.isFile() ? "EXISTS" : "NOT FOUND'"));

            if (f.isFile()) {
                try (final InputStream in = new FileInputStream(f)) {
                    final Properties res = new Properties();
                    res.load(in);

                    props = res;
                } catch (Exception e) {
                    throw new RuntimeException(e.getMessage(), e);
                }
            } else {
                props = new Properties();
            }
        }

        return props;
    }

    static String getMongoHost(String def) {
        return def == null ? getKey(KEY_HOST, ServerAddress.defaultHost()) : getKey(KEY_HOST, def);
    }

    static int getMongoPort(int def) {
        return def == -1 ? getKey(KEY_PORT, ServerAddress.defaultPort()) : getKey(KEY_PORT, def);
    }

    static String getMongoUsername(String def) {
        return getKey(KEY_USER, def);
    }

    static String getMongoPassword(String def) {
        return getKey(KEY_PASS, def);
    }

    private static String getKey(String key, String def) {
        String res = getProperties().getProperty(key);

        if (res == null) {
            res = System.getenv(key);
        }

        return res == null ? def : res;
    }

    private static int getKey(String key, int def) {
        String res = getProperties().getProperty(key);

        if (res == null) {
            res = System.getenv(key);
        }

        return res == null ? def : Integer.parseInt(res);
    }
}
