/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.commons.mongodb;

import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.util.*;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 12/09/18
 */
public class MongoDbWrapper {
    // mongo server connection
    private final String mongoHost;
    private final int mongoPort;
    private final String mongoUser;
    private final String mongoPass;
    private final boolean reuse;

    // db connection
    private MongoClient mongoClient;
    private MongoDatabase db;

    private final String digest;
    // set from outside - connect to this db/connection/fields
    private final String database;
    private final String collection;
    private final String userField;
    private final String passwordField;
    private final String roleField;

    public MongoDbWrapper(
            final String mongoHost,
            final int mongoPort,
            final String mongoUser,
            final String mongoPass,
            final String database,
            final String collection,
            final String userField,
            final String passwordField,
            final String roleField,
            final String digest,
            final boolean reuse) {
        this.mongoHost = mongoHost;
        this.mongoPort = mongoPort;
        this.mongoUser = mongoUser;
        this.mongoPass = mongoPass;
        this.reuse = reuse;

        this.digest = digest;

        Utils.paramRequired("database", database);
        this.database = database;

        Utils.paramRequired("collection", collection);
        this.collection = collection;

        Utils.paramRequired("userField", userField);
        this.userField = userField;

        Utils.paramRequired("passwordField", passwordField);
        this.passwordField = passwordField;

        Utils.paramRequired("roleField", roleField);
        this.roleField = roleField;
    }

    public void start() {
        if (mongoClient != null) {
            stop();
        }

        this.mongoClient = reuse ?
                Factory.getMongoClient(
                        Config.getMongoHost(mongoHost),
                        Config.getMongoPort(mongoPort),
                        Config.getMongoUsername(mongoUser),
                        Config.getMongoPassword(mongoPass),
                        database) :
                Factory.newMongoClient(
                        Config.getMongoHost(mongoHost),
                        Config.getMongoPort(mongoPort),
                        Config.getMongoUsername(mongoUser),
                        Config.getMongoPassword(mongoPass),
                        database);

        db = mongoClient.getDatabase(database);
    }

    public void stop() {
        if (mongoClient != null && !reuse)  {
            mongoClient.close();
        }

        mongoClient = null;
        db = null;
    }

    @SuppressWarnings("unchecked")
    public Collection<String> getRoles(final String username, final String password) {
        final MongoCollection<Document> docs = db.getCollection(collection);

        BasicDBObject whereQuery = new BasicDBObject();
        whereQuery.put(userField, username);
        Document doc = docs.find(whereQuery).first();

        if (doc == null) {
            return null;
        }

        final String[] passwords = getPasswords(username);

        if (passwords == null) {
            return null;
        }

        boolean found = false;
        for (final String retrievedPassword: passwords) {
            final String checkPassword = digest == null ? password : Utils.getDigest(digest, password);

            if (Objects.equals(retrievedPassword, checkPassword)) {
                found = true;
            }
        }

        if (!found) {
            return null;
        }

        final List<String> res = doc.get(roleField, List.class);

        return res == null ? null : new HashSet<>(res);
    }

    public String[] getPasswords(final String username) {
        final Collection<String> res = new HashSet<>();

        final MongoCollection<Document> docs = db.getCollection(collection);

        final BasicDBObject whereQuery = new BasicDBObject();
        whereQuery.put(userField, username);
        final Document doc = docs.find(whereQuery).first();

        if (doc != null) {
            final String[] fields = passwordField.split(",");

            for (String field: fields) {
                field = field.trim();

                Object o = doc.get(field);

                if (o != null) {
                    res.add(o.toString());
                }
            }
        }

        return res.isEmpty() ? null : res.toArray(new String[0]);
    }

    public Collection<String> getRoles(final String username) {
        final MongoCollection<Document> docs = db.getCollection(collection);

        BasicDBObject whereQuery = new BasicDBObject();
        whereQuery.put(userField, username);
        Document doc = docs.find(whereQuery).first();

        return doc == null ? new HashSet<String>() : new HashSet<String>(doc.get(roleField, List.class));
    }
}
