/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.commons.mongodb;

import java.security.MessageDigest;
import java.util.logging.LogManager;
import java.util.logging.Logger;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 13/09/18
 */
public class Utils {
    private final static char[] HEX = "0123456789abcdef".toCharArray();

    static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = HEX[v >>> 4];
            hexChars[j * 2 + 1] = HEX[v & 0x0F];
        }
        return new String(hexChars);
    }

    public static Logger getLogger(final Class<?> clazz) {
        final Logger res = LogManager.getLogManager().getLogger(clazz.getName());

        return res == null ? LogManager.getLogManager().getLogger("") : res;
    }

    static Logger getLogger(Object o) {
        return o == null ? getLogger(Utils.class) : getLogger(o.getClass());
    }

    static void paramRequired(final String name, final String value) {
        if (value == null) {
            throw new IllegalArgumentException(String.format("%s parameter is null", name));
        }
    }

    static String getDigest(final String digest, final String input) {
        try {
            MessageDigest md = MessageDigest.getInstance(digest);
            return Utils.bytesToHex(md.digest(input.getBytes()));
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    /**
     * Generate a unique id from an array of string.
     *
     * @param data the array of string
     * @return a unique id from an array of string.
     */
    static String generateId(String... data) {
        final StringBuilder sb = new StringBuilder();

        if (data != null) {
            for (final String s: data) {
                sb.append(String.format("/%s", s == null ? "" : s));
            }
        }

        return sb.toString();
    }

}
