/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.commons.mongodb;

import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;

import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 13/09/18
 */
public class Factory {
    private static final Logger LOG = Utils.getLogger(Factory.class);

    private static final Map<String, MongoClient> CLIENTS = new ConcurrentHashMap<>();

    static MongoClient getMongoClient(
            final String host, final int port, final String username, final String password, final String database) {
        Utils.paramRequired("host", host);
        Utils.paramRequired("database", database);

        if (username == null && password != null) {
            LOG.log(
                    Level.WARNING,
                    "username parameter is null but password isn't, nevertheless neither won't be used to login to mongodb until both are set.");
        }

        if (username != null && password == null) {
            LOG.log(
                    Level.WARNING,
                    "username parameter is set but password is null, nevertheless neither won't be used to login to mongodb until both are set.");
        }

        final String id = Utils.generateId(
                host, username, password == null ? null : Utils.getDigest("md5", password), database);

        final MongoClient res;

        if (!CLIENTS.containsKey(id)) {
            res = newMongoClient(host, port, username, password, database);
            CLIENTS.put(id, res);
        } else {
            res = CLIENTS.get(id);
        }

        return res;
    }

    static MongoClient newMongoClient(
            final String host, final int port, final String username, final String password, final String database) {
        Utils.paramRequired("host", host);
        Utils.paramRequired("database", database);

        if (username == null && password != null) {
            LOG.log(
                    Level.WARNING,
                    "username parameter is null but password isn't, nevertheless neither won't be used to login to mongodb until both are set.");
        }

        if (username != null && password == null) {
            LOG.log(
                    Level.WARNING,
                    "username parameter is set but password is null, nevertheless neither won't be used to login to mongodb until both are set.");
        }

        if (username != null && password != null) {
            MongoCredential credential = MongoCredential.createCredential(
                    username, database, password.toCharArray());
            return new MongoClient(new ServerAddress(host, port), Collections.singletonList(credential));
        } else {
            return new MongoClient(host, port);
        }
    }
}
